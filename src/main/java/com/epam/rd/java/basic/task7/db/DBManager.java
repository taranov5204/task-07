package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static final String FIND_ALL_USERS_SQL =
            "SELECT * FROM users";

    private static final String CREATE_USER_SQL =
            "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";

    private static final String DELETE_USER_SQL =
            "delete from users where id=?";

    private static final String GET_USER_SQL =
            "select * from users where login=?";

    private static final String GET_TEAM_SQL =
            "select * from teams where name=?";

    private static final String GET_TEAM_ID_SQL =
            "select * from teams where name=?";

    private static final String FIND_ALL_TEAMS_SQL =
            "select * from teams";

    private static final String CREATE_TEAM_SQL =
            "insert into teams (id, name) values (default, ?)";

    private static final String DELETE_TEAM_SQL =
            "delete from teams where id=?";

    private static final String SET_TEAMS_FOR_USER_SQL =
            "insert into users_teams values (?, ?)";

    private static final String GET_USER_TEAMS_SQL =
            "select name " +
                    "from teams t, users u, users_teams ut where u.id=ut.user_id and t.id=ut.team_id and ut.user_id=?";

    private static final String UPDATE_TEAM_SQL =
            "update teams set name=? where id=?";

    private static DBManager instance;
    private Connection connection;
    private static final String URL = "jdbc:derby:memory:testdb;create=true";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "123123";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }


    public Connection getConnection() {
        try {
            Properties properties = new Properties();
            FileInputStream fis = new FileInputStream("app.properties");
            properties.load(fis);
            String url = properties.getProperty("connection.url");
            connection = DriverManager.getConnection(url);
            connection.setAutoCommit(false);
            //Class.forName("org.postgresql.Driver");
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return connection;
    }


    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new LinkedList<>();
        Connection c = getInstance().getConnection();
        try {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(FIND_ALL_USERS_SQL);

            while (rs.next()) {
                users.add(extractUser(rs));
            }
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(c);
        }
        return users;
    }

    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }


    public boolean insertUser(User user) throws DBException {
        Connection c = getInstance().getConnection();
        try {

            PreparedStatement pstmt = c.prepareStatement(CREATE_USER_SQL);
            pstmt.setString(1, user.getLogin());
            pstmt.executeUpdate();
            c.commit();
        } catch (
                SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot insert user", e);
        } finally {
            close(c);
        }
        return true;
    }

    private int getTeamIdByName(String s) {
        Connection c = null;
        int res = 0;
        try {
            c = getInstance().getConnection();
            PreparedStatement ps = c.prepareStatement(GET_TEAM_ID_SQL);
            ps.setString(1, s);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                res = rs.getInt(1);
            }
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(c);
        }
        return res;
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection c = getInstance().getConnection();
        try {
            for (User user : users) {
                deleteUser(user.getId(), c);
            }
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot delete user", e);
        } finally {
            close(c);
        }
        return true;
    }

    private boolean deleteUser(int userID, Connection c) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(DELETE_USER_SQL);
        pstmt.setInt(1, userID);
        pstmt.executeUpdate();
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        Connection c = getInstance().getConnection();
        try {
            PreparedStatement pstmt = c.prepareStatement(GET_USER_SQL);
            pstmt.setString(1, login);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                user = extractUser(rs);
            }
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get user", e);
        } finally {
            close(c);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        Connection c = getInstance().getConnection();
        try {
            PreparedStatement pstmt = c.prepareStatement(GET_TEAM_SQL);
            pstmt.setString(1, name);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                team = extractTeam(rs);
            }
            c.commit();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Cannot get team", e);
        } finally {
            close(c);
        }
        return team;
    }

    private Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new LinkedList<>();
        Connection c = getInstance().getConnection();
        try {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(FIND_ALL_TEAMS_SQL);

            while (rs.next()) {
                teams.add(extractTeam(rs));
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(c);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection c = getInstance().getConnection();
        try {
            PreparedStatement pstmt = c.prepareStatement(CREATE_TEAM_SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, team.getName());
            if (pstmt.executeUpdate() > 0) {
                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
            c.commit();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot insert team", e);
        } finally {
            close(c);
        }
        return true;
    }


    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection c = getInstance().getConnection();
        try {
            user = getUser(user.getLogin());
            PreparedStatement pstmt = c.prepareStatement(SET_TEAMS_FOR_USER_SQL);
            pstmt.setInt(1, user.getId());
            for (Team team : teams) {
                pstmt.setInt(2, getTeamIdByName(team.getName()));
                pstmt.executeUpdate();
            }
            c.commit();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot set team for user", e);
        } finally {
            close(c);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = new ArrayList<>();
        Connection c = getInstance().getConnection();
        try {
            user = getUser(user.getLogin());
            PreparedStatement pstmt = c.prepareStatement(GET_USER_TEAMS_SQL);
            pstmt.setInt(1, user.getId());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(new Team(rs.getString("name")));
            }
            c.commit();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot get user teams", e);
        } finally {
            close(c);
        }

        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection c = getInstance().getConnection();
        try {
            PreparedStatement pstmt = c.prepareStatement(DELETE_TEAM_SQL);
            pstmt.setInt(1, getTeamIdByName(team.getName()));
            pstmt.executeUpdate();
            c.commit();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot delete team", e);
        } finally {
            close(c);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection c = getInstance().getConnection();
        try {
            PreparedStatement pstmt = c.prepareStatement(UPDATE_TEAM_SQL);
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            pstmt.executeUpdate();
            c.commit();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(c);
            throw new DBException("Cannot update team", e);
        } finally {
            close(c);
        }
        return true;
    }

    public static void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
