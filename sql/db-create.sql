
DROP TABLE IF EXISTS users_teams;

DROP TABLE IF EXISTS users;

DROP TABLE IF EXISTS teams;


CREATE SEQUENCE user_id_seq;
CREATE TABLE users (
                       id INT NOT NULL PRIMARY KEY DEFAULT nextval('user_id_seq'),
                       login VARCHAR(10)
);
ALTER SEQUENCE user_id_seq OWNED BY users.id;

CREATE SEQUENCE team_id_seq;
CREATE TABLE teams (
                       id INT NOT NULL PRIMARY KEY DEFAULT nextval('team_id_seq'),
                       name VARCHAR(10)
);
ALTER SEQUENCE team_id_seq OWNED BY teams.id;

CREATE TABLE users_teams (
                             user_id INT REFERENCES users(id) on delete cascade,
                             team_id INT REFERENCES teams(id) on delete cascade
);

INSERT INTO users (login) VALUES ('ivanov');
INSERT INTO teams (name) VALUES ('teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;
